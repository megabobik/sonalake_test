package io.example.rest.controllers;

import io.example.AppMain;
import io.example.config.SurferDayUpdater;
import io.example.models.SurferDayModel;
import io.example.persistence.repo.CountryRepository;
import io.example.persistence.services.SurferDayService;
import io.example.rest.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppMain.class)
@TestPropertySource("classpath:application.properties")
@AutoConfigureMockMvc
public class ServiceTest {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SurferDayService surferDayService;

    @Test
    public void Test() throws Exception {
        SurferDayModel surferDayModel = new SurferDayModel();
        surferDayModel.setWindSpeed("5.3");
        surferDayModel.setAverageTemp("36.6");
        surferDayModel.setLocationCity("Antarctica");
        surferDayModel.setLocationCountryCode("AC");

        when(surferDayService.findBestLocationForSurfingByDay(LocalDate.of(2020, 1, 20))).thenReturn(surferDayModel);

        mockMvc.perform(get("/api/windsurfer/best?date=2020-01-20")).andExpect(status().isOk()).andExpect(content().string(containsString("Antarctica")));
    }

}
