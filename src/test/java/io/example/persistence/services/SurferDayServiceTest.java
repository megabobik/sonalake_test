package io.example.persistence.services;

import io.example.AppMain;
import io.example.config.SurferDayUpdater;
import io.example.models.SurferDayModel;
import io.example.persistence.entities.CountryEntity;
import io.example.persistence.repo.CountryRepository;
import io.example.persistence.services.SurferDayService;
import io.example.rest.WeatherConditionModel;
import io.example.rest.WeatherService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppMain.class)
public class SurferDayServiceTest {

	@MockBean
	private WeatherService weatherService;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private SurferDayService surferDayService;

	@Autowired
	private SurferDayUpdater surferDayUpdater;

	@Test
	public void test_persisting_best_surfer_location_per_day() {
		CountryEntity entity = new CountryEntity();
		entity.setLocationName("Miami");
		entity.setCountryCode("US");
		entity.setVersion(1);

		countryRepository.save(entity);

		entity = new CountryEntity();
		entity.setLocationName("Tenerife");
		entity.setCountryCode("SP");
		entity.setVersion(1);

		countryRepository.save(entity);

		Map<LocalDate, WeatherConditionModel> modelMap = new HashMap<>();
		modelMap.put(LocalDate.of(2020, 1, 10), new WeatherConditionModel(5.3, 31.3, "Miami", "US"));
		modelMap.put(LocalDate.of(2020, 1, 11), new WeatherConditionModel(6.3, 24.3, "Miami", "US"));
		modelMap.put(LocalDate.of(2020, 1, 12), new WeatherConditionModel(5.3, 24.3, "Miami", "US"));
		modelMap.put(LocalDate.of(2020, 1, 13), new WeatherConditionModel(5.2, 24.3, "Miami", "US"));
		modelMap.put(LocalDate.of(2020, 1, 14), new WeatherConditionModel(7.3, 24.3, "Miami", "US"));
		when(weatherService.getDataFromWeatherAPI("Miami", "US")).thenReturn(modelMap);

		Map<LocalDate, WeatherConditionModel> modelMap1 = new HashMap<>();
		modelMap1.put(LocalDate.of(2020, 1, 10), new WeatherConditionModel(6.3, 24.3, "Tenerife", "SP"));
		modelMap1.put(LocalDate.of(2020, 1, 11), new WeatherConditionModel(1.3, 24.3, "Tenerife", "SP"));
		modelMap1.put(LocalDate.of(2020, 1, 12), new WeatherConditionModel(8.3, 22.3, "Tenerife", "SP"));
		modelMap1.put(LocalDate.of(2020, 1, 13), new WeatherConditionModel(9.3, 24.3, "Tenerife", "SP"));
		modelMap1.put(LocalDate.of(2020, 1, 14), new WeatherConditionModel(6.7, 24.3, "Tenerife", "SP"));
		when(weatherService.getDataFromWeatherAPI("Tenerife", "SP")).thenReturn(modelMap1);

		surferDayUpdater.updateData();

		SurferDayModel surferDayModel = surferDayService.findBestLocationForSurfingByDay(LocalDate.of(2020, 1 ,10));

		Assert.assertEquals(surferDayModel.getWindSpeed(), "5.3");
	}
}