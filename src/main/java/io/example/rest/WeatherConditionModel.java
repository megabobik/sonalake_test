package io.example.rest;

public class WeatherConditionModel implements Comparable<WeatherConditionModel>{

    private double windSpeed;
    private double temperature;
    private String locationName;
    private String countryCode;

    public WeatherConditionModel(double windSpeed, double temperature, String locationName, String countryCode) {
        this.windSpeed = windSpeed;
        this.temperature = temperature;
        this.locationName = locationName;
        this.countryCode = countryCode;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public double getTemperature() {
        return temperature;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public boolean isSuitable() {
        if(5 <= windSpeed && windSpeed <= 18) {
            if(5 <= temperature && temperature <= 35) {
                return true;
            }
        }
        return false;
    }

    public double getValue() {
        if(5 <= windSpeed && windSpeed <= 18) {
            if(5 <= temperature && temperature <= 35) {
                return windSpeed * 3 + temperature;
            }
        }
        return -999;
    }

    @Override
    public int compareTo(WeatherConditionModel o) {
        System.out.println(" >>> " + this.getValue() + " _ " + o.getValue());
        if(this.getValue() > o.getValue()) {
            return 1;
        } else if(this.getValue() < o.getValue()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "WeatherConditionModel{" +
                "windSpeed=" + windSpeed +
                ", temperature=" + temperature +
                ", locationName='" + locationName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}