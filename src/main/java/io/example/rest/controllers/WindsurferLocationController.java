package io.example.rest.controllers;

import io.example.models.SurferDayModel;
import io.example.persistence.services.SurferDayService;
import io.example.rest.model.BestWSLocationResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/windsurfer")
public class WindsurferLocationController {

    @Autowired
    private SurferDayService surferDayService;

    @GetMapping(value = "/best", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity best(@RequestParam("date") String date) {
        LocalDate parsedDate = null;

        try {
            parsedDate = LocalDate.parse(date);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        if(parsedDate.isAfter(LocalDate.now().plusDays(15))) {
            return ResponseEntity.badRequest().build();
        }

        SurferDayModel surfDayModel = surferDayService.findBestLocationForSurfingByDay(parsedDate);

        BestWSLocationResponseDTO dto = new BestWSLocationResponseDTO();
        dto.setAverageTemp(surfDayModel.getAverageTemp());
        dto.setWindSpeed(surfDayModel.getWindSpeed());
        dto.setLocationCity(surfDayModel.getLocationCity());
        dto.setLocationCountryCode(surfDayModel.getLocationCountryCode());

        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
}