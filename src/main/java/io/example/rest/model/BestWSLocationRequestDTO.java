package io.example.rest.model;

public class BestWSLocationRequestDTO {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
