package io.example.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class WeatherService {

    @Value("${weather.api}")
    private String apiKey;

    private String VALID_TIME_NODE_NAME = "valid_date";
    private String WIND_SPEED_NODE_NAME = "wind_spd";
    private String TEMPERATURE_NODE_NAME = "temp";

    public Map<LocalDate, WeatherConditionModel> getDataFromWeatherAPI(String city, String countryCode) {
        Map<LocalDate, WeatherConditionModel> map = new HashMap<>();

        String URL = String.format("https://api.weatherbit.io/v2.0/forecast/daily?key=%s&city=%s&country=%s", apiKey, city, countryCode);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(URL, String.class);

        if(response.getStatusCode() == HttpStatus.NO_CONTENT) {
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        try {
            rootNode = mapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.out.println("content is null for " + city + countryCode);
            e.printStackTrace();
        }
        JsonNode dataNode = rootNode.path("data");

        Iterator<JsonNode> iter = dataNode.elements();
        while (iter.hasNext()) {
            JsonNode nod = iter.next();

            LocalDate date = LocalDate.parse(nod.get(VALID_TIME_NODE_NAME).asText());
            double wind = nod.get(WIND_SPEED_NODE_NAME).asDouble();
            double temp = nod.path(TEMPERATURE_NODE_NAME).asDouble();

            map.put(date, new WeatherConditionModel(wind, temp, city, countryCode));
        }

        return map;
    }
}