package io.example.models;

import io.example.persistence.entities.SurferDayEntity;
import org.springframework.stereotype.Component;

@Component
public class SurferDayMapper {

    public SurferDayModel map(SurferDayEntity entity) {
        SurferDayModel surferDay = new SurferDayModel();

        surferDay.setAverageTemp(entity.getAverageTemp());
        surferDay.setWindSpeed(entity.getWindSpeed());
        surferDay.setLocationCity(entity.getLocation().getLocationName());
        surferDay.setLocationCountryCode(entity.getLocation().getCountryCode());

        return surferDay;
    }
}