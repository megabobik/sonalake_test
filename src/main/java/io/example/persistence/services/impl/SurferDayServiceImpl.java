package io.example.persistence.services.impl;

import io.example.models.SurferDayMapper;
import io.example.models.SurferDayModel;
import io.example.persistence.entities.CountryEntity;
import io.example.persistence.entities.SurferDayEntity;
import io.example.persistence.repo.CountryRepository;
import io.example.persistence.repo.SurferDayRepository;
import io.example.persistence.services.SurferDayService;
import io.example.rest.WeatherConditionModel;
import io.example.rest.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SurferDayServiceImpl implements SurferDayService {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private SurferDayRepository surferDayRepository;

    @Autowired
    private SurferDayMapper surferDayMapper;

    @Override
    public SurferDayModel findBestLocationForSurfingByDay(LocalDate date) {
        int countryMaxVersion = countryRepository.findMaxVersion();
        SurferDayEntity surferDayEntity = surferDayRepository.findByDateAndVersion(date, countryMaxVersion);
        return surferDayMapper.map(surferDayEntity);
    }

    @Override
    public void updateBestSurferLocationPerDate() {
        int countryVersion = countryRepository.findMaxVersion();
        int datesVersion = -1;
        if(surferDayRepository.findMaxVersion() != null)  {
            datesVersion = surferDayRepository.findMaxVersion();
        }

        if(countryVersion == datesVersion) {
            return;
        }

        List<CountryEntity> countryList = countryRepository.findAllWithMaxVersion();

        Map<LocalDate, List<WeatherConditionModel>> map = new HashMap<>();

        for(CountryEntity countryEntity : countryList) {
            Map<LocalDate, WeatherConditionModel> dates = weatherService
                    .getDataFromWeatherAPI(countryEntity.getLocationName(), countryEntity.getCountryCode());

            if(dates == null) {
                continue;
            }
            for(Map.Entry<LocalDate, WeatherConditionModel> entry: dates.entrySet()) {

                List<WeatherConditionModel> weatherConditionModelList;

                if(map.containsKey(entry.getKey())) {
                    weatherConditionModelList = map.get(entry.getKey());
                } else {
                    weatherConditionModelList = new ArrayList<>();
                    map.put(entry.getKey(), weatherConditionModelList);
                }
                weatherConditionModelList.add(entry.getValue());
            }
        }

        for (LocalDate entry : map.keySet()) {
            List<WeatherConditionModel> weatherConditionModelList = map.get(entry);
            WeatherConditionModel weatherConditionModel = weatherConditionModelList
                    .stream()
                    .filter(WeatherConditionModel::isSuitable)
                    .sorted(Comparator.comparing(WeatherConditionModel::getValue, Comparator.reverseOrder()))
                    .collect(Collectors.toList()).get(0);

            CountryEntity countryEntity = countryRepository
                    .findByLocationNameAndCountryCode(weatherConditionModel.getLocationName(), weatherConditionModel.getCountryCode());

            SurferDayEntity result = new SurferDayEntity();
            result.setLocation(countryEntity);
            result.setDate(entry);
            result.setAverageTemp(Double.toString(weatherConditionModel.getTemperature()));
            result.setWindSpeed(Double.toString(weatherConditionModel.getWindSpeed()));
            result.setVersion(countryVersion);

            surferDayRepository.save(result);
        }
    }
}