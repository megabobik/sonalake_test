package io.example.persistence.services;

import io.example.models.SurferDayModel;

import java.time.LocalDate;

public interface SurferDayService {

    SurferDayModel findBestLocationForSurfingByDay(LocalDate date);

    void updateBestSurferLocationPerDate();
}