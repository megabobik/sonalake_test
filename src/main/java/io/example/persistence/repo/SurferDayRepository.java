package io.example.persistence.repo;

import io.example.persistence.entities.SurferDayEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;


@Repository
public interface SurferDayRepository extends JpaRepository<SurferDayEntity, Long> {

    SurferDayEntity findByDateAndVersion(LocalDate date, int version);

    @Query("SELECT max(c.version) FROM surfdays c")
    Integer findMaxVersion();

}