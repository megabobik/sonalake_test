package io.example.persistence.repo;

import io.example.persistence.entities.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<CountryEntity, Long> {

    @Query("SELECT max(c.version) FROM countries c")
    Integer findMaxVersion();

    @Query("FROM countries c where c.version = (SELECT max(c.version) FROM countries c)")
    List<CountryEntity> findAllWithMaxVersion();

    List<CountryEntity> findAllByVersion(int version);

    CountryEntity findByLocationNameAndCountryCode(String locationName, String countryCode);

}