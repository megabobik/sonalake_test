package io.example.config;

import io.example.persistence.entities.CountryEntity;
import io.example.persistence.repo.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PreloadMain {

    private static final Logger logger = LoggerFactory.getLogger(PreloadMain.class);

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private SurferDayUpdater surferDayUpdater;

    @Bean
    public CommandLineRunner init() {
        return args -> {
            
            //test data
            CountryEntity entity = new CountryEntity();
            entity.setLocationName("Jastarnia");
            entity.setCountryCode("PL");
            entity.setVersion(1);

            countryRepository.save(entity);

            entity = new CountryEntity();
            entity.setLocationName("Bridgetown");
            entity.setCountryCode("BB");
            entity.setVersion(1);

            countryRepository.save(entity);

            entity = new CountryEntity();
            entity.setLocationName("Fortaleza");
            entity.setCountryCode("BR");
            entity.setVersion(1);

            countryRepository.save(entity);

            entity = new CountryEntity();
            entity.setLocationName("Pissouri");
            entity.setCountryCode("CY");
            entity.setVersion(1);

            countryRepository.save(entity);

            entity = new CountryEntity();
            entity.setLocationName("LeMorne");
            entity.setCountryCode("MU");
            entity.setVersion(1);

            countryRepository.save(entity);

            surferDayUpdater.updateData();
        };
    }
}