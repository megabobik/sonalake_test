package io.example.config;

import io.example.persistence.services.SurferDayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SurferDayUpdater {

    private static final Logger logger = LoggerFactory.getLogger(SurferDayUpdater.class);

    @Autowired
    private SurferDayService surferDayService;

    @Scheduled(cron = "0 1 * * * ?")
    public void updateData() {
        logger.info("Starting update best locations for surfing per day");
        surferDayService.updateBestSurferLocationPerDate();
        logger.info("Best locations for surfing per day updated succesfully");
    }
}